package cajero;

public class Tarjeta {
	private int id;
	private String numeroDeTarjeta;
	private int nip;
	private double fondos;
	private int usuarioId;
	
	public Tarjeta() {
		
	}
	
	public Tarjeta(int id, String numTarjeta, int nip, double fondos, int usuario) {
		
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNumeroDeTarjeta() {
		return numeroDeTarjeta;
	}

	public void setNumeroDeTarjeta(String numeroDeTarjeta) {
		this.numeroDeTarjeta = numeroDeTarjeta;
	}

	public int getNip() {
		return nip;
	}

	public void setNip(int nip) {
		this.nip = nip;
	}

	public double getFondos() {
		return fondos;
	}

	public void setFondos(double fondos) {
		this.fondos = fondos;
	}

	public int getUsuarioId() {
		return usuarioId;
	}

	public void setUsuarioId(int usuarioId) {
		this.usuarioId = usuarioId;
	}

	@Override
	public String toString() {
		return "Tarjeta {id:" + id + ", numeroDeTarjeta:" + numeroDeTarjeta + ", nip:" + nip + ", fondos:" + fondos
				+ ", usuarioId:" + usuarioId + "}";
	}
	
	
}
