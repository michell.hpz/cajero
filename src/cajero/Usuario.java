package cajero;

public class Usuario {
	private int id;
	private String nombre, apellidoPaterno, apellidoMaterno;

	public Usuario() {
		
	}
	
	public Usuario(int id, String nom, String apePat, String apeMat) {
		
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellidoPaterno() {
		return apellidoPaterno;
	}

	public void setApellidoPaterno(String apellidoPaterno) {
		this.apellidoPaterno = apellidoPaterno;
	}

	public String getApellidoMaterno() {
		return apellidoMaterno;
	}

	public void setApellidoMaterno(String apellidoMaterno) {
		this.apellidoMaterno = apellidoMaterno;
	}

	@Override
	public String toString() {
		return "Usuario {id:" + id + ", nombre completo:" + nombre + " " + apellidoPaterno
				+ " " + apellidoMaterno + "}";
	}
	
	
	
}
