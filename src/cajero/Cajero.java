package cajero;

public class Cajero {
	private static Conexion c = new Conexion();
	private static Usuario u;
	private static Tarjeta t;
	private Tarjeta tarjetaUsuario;
	private Usuario usuario;
	
	private static final String MENSAJE_VALIDACION_DE_TARJETA = "--- Validar Tarjeta antes de cualquier operación ---";
	public static final String ERROR_VALIDACION_DE_TARJETA = "--- La tarjeta y/o NIP son incorrectos ---";
	
	public boolean validarTarjetaConNip(String numTarjeta, int nip) {
		t = new Tarjeta();
		t.setNumeroDeTarjeta(numTarjeta);
		tarjetaUsuario = c.seleccionarTarjeta(t);
		if(tarjetaUsuario != null) {
			if(tarjetaUsuario.getNip() == nip) {
				return true;
			}
		}
		System.out.println(ERROR_VALIDACION_DE_TARJETA);
		return false;
	}
	
	public String consultaNombreUsuario() {
		actualizarDatosTarjeta();
		if(tarjetaUsuario != null) {
			u = new Usuario();
			u.setId(tarjetaUsuario.getUsuarioId());
			usuario = c.seleccionarUsuario(u);
			return usuario.getNombre() + " " + usuario.getApellidoPaterno() + " " + usuario.getApellidoMaterno();
		}
		return null;
	}
	
	public double consultarFondos() {
		actualizarDatosTarjeta();
		if(tarjetaUsuario != null) {
			return tarjetaUsuario.getFondos();
		}
		System.out.println(MENSAJE_VALIDACION_DE_TARJETA);
		return 0.0;
	}
	
	public boolean depositarDinero(double cantidadADepositar) {
		actualizarDatosTarjeta();
		if(tarjetaUsuario != null) {
			if(cantidadADepositar > 0) {
				double fondos = tarjetaUsuario.getFondos() + cantidadADepositar;
				c.actualizarTarjeta(tarjetaUsuario.getId(), fondos);
				return true;
			}
			System.out.println("--- Ingresa una cantidad valida ---");
			return false;
		}
		System.out.println(MENSAJE_VALIDACION_DE_TARJETA);
		return false;
	}
	
	public boolean retirarFondos(double cantidadARetirar) {
		actualizarDatosTarjeta();
		if(tarjetaUsuario != null) {
			if(cantidadARetirar > 0) {
				if(cantidadARetirar <= tarjetaUsuario.getFondos()) {
					double fondos = tarjetaUsuario.getFondos() - cantidadARetirar;
					c.actualizarTarjeta(tarjetaUsuario.getId(), fondos);
					return true;
				}
				System.out.println("--- No cuentas con los fondos suficientes ---");
				return false;
			}
			System.out.println("--- Ingresa una cantidad valida ---");
			return false;
		}
		System.out.println(MENSAJE_VALIDACION_DE_TARJETA);
		return false;
	}
	
	public static void crearUsaurio(String nombre, String apePat, String apeMat, String numTarjeta, int nip, double fondoInicial) {
		//Crear instancias de usuario y tarjeta
		u = new Usuario();
		t = new Tarjeta();
		
		// generar id autoincrementable para usuario y tarjeta
		int idUsuario = c.contarRegistrosUsuarios() + 1;
		int idTarjeta = c.contarRegistrosTarjetas() + 1;
		
		// colocar datos del usuario al objeto u
		u.setId(idUsuario);
		u.setNombre(nombre);
		u.setApellidoPaterno(apePat);
		u.setApellidoMaterno(apeMat);
		
		// colocar los datos de la tarjeta en el objeto t
		t.setId(idTarjeta);
		t.setNumeroDeTarjeta(numTarjeta);
		t.setNip(nip);
		t.setFondos(fondoInicial);
		t.setUsuarioId(idUsuario);
		
		// Guardar los datos de la tarjeta y el usuario en la bd
		c.insertarUsuario(u);
		c.insertarTarjeta(t);
	}
	
	private void actualizarDatosTarjeta() {
		t = new Tarjeta();
		t.setId(tarjetaUsuario.getId());
		tarjetaUsuario = c.seleccionarTarjeta(t);
	}
}
