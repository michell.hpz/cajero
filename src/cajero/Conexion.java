package cajero;
import com.db4o.Db4oEmbedded;
import com.db4o.ObjectContainer;
import com.db4o.ObjectSet;

public class Conexion {
	private ObjectContainer db = null;
	
	private void abrirRegistro() {
		db = Db4oEmbedded.openFile("BancoTest");
	}
	
	private void cerrarRegistro() {
		db.close();
	}
	
	public void insertarUsuario(Usuario u) {
		abrirRegistro();
		db.store(u);
		cerrarRegistro();
	}
	
	public void insertarTarjeta(Tarjeta t) {
		abrirRegistro();
		db.store(t);
		cerrarRegistro();
	}
	
	public Usuario seleccionarUsuario(Usuario u) {
		try {
			abrirRegistro();
			ObjectSet resultado = db.queryByExample(u);
			if(resultado.hasNext()) {
				Usuario usuario = (Usuario) resultado.next();
				cerrarRegistro();
				return usuario;				
			}
		}catch(Exception e) {
			System.out.println("--- Ha ocurrido un error al SELECCIONAR UN USUARIO ---");
		}
		cerrarRegistro();
		return null;
	}
	
	public Tarjeta seleccionarTarjeta(Tarjeta t) {
		try {
			abrirRegistro();
			ObjectSet resultado = db.queryByExample(t);
			if(resultado.hasNext()) {
				Tarjeta tarjeta = (Tarjeta) resultado.next();
				cerrarRegistro();
				return tarjeta;				
			}
		}catch(Exception e) {
			System.out.println("--- Ha ocurrido un error al SELECCIONAR UNA TARJETA ---");
		}
		cerrarRegistro();
		return null;
	}
	
	public boolean actualizarTarjeta(int id, double fondos) {
		try {
			abrirRegistro();
			Tarjeta tarjeta = new Tarjeta();
			tarjeta.setId(id);
			ObjectSet resultado = db.queryByExample(tarjeta);
			if(resultado.hasNext()) {
				Tarjeta preResultado = (Tarjeta) resultado.next();
				preResultado.setFondos(fondos);
				db.store(preResultado);
				cerrarRegistro();
				return true;
			}
		}catch(Exception e) {
			System.out.println("--- Ha ocurrido un error al ACTUALIZAR UNA TARJETA ---");
		}
		cerrarRegistro();
		return false;
	}
	
	public int contarRegistrosUsuarios() {
		try {
			abrirRegistro();
			ObjectSet usuarios = db.queryByExample(Usuario.class);
			if(usuarios.hasNext()) {
				int cantidadUsuarios = usuarios.size();
				// System.out.println(cantidadUsuarios);
				cerrarRegistro();
				return  cantidadUsuarios;				
			}
		}catch(Exception e) {
			System.out.println("--- Ha ocurrido un error al CONTAR REGISTROS ---");
		}
		cerrarRegistro();
		return 0;
	}
	
	public int contarRegistrosTarjetas() {
		try {
			abrirRegistro();
			ObjectSet tarjetas = db.queryByExample(Tarjeta.class);
			if(tarjetas.hasNext()) {
				int cantidadTarjetas = tarjetas.size();
				// System.out.println(cantidadTarjetas);
				cerrarRegistro();
				return cantidadTarjetas;				
			}
		}catch(Exception e) {
			System.out.println("--- Ha ocurrido un error al CONTAR REGISTROS ---");
		}
		cerrarRegistro();
		return 0;
	}
	
}
