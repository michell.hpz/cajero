package cajero;
import java.util.Scanner;
public class PrincipalCajero {
	private static Scanner lee =  new Scanner(System.in);
	
	public static void main(String[] args) {
		boolean MenuPrincipal = true;
		
		do {
			imprimirMenuPrincipal();
			int resp = lee.nextInt();
			switch(resp){
				case 1:
					crearUsuarioConTarjeta();
					break;
				case 2:
					abrirCajero();
					break;
				case 3:
					MenuPrincipal = false;
					break;
				default:
					System.out.println("Esa opci�n no existe, si quieres salir elige 3");
			}
			
		}while(MenuPrincipal);
		
	}

	private static void crearUsuarioConTarjeta() {
		String nombre = "", apePat ="", apeMat ="", numTarjeta ="";
		int nip = 0;
		double fondoInicial = 0;
		
		System.out.println("--- Ingresa los datos del usuario ---");
		System.out.print("Nombre: ");
		nombre = lee.next();
		System.out.print("Apellido Paterno: ");
		apePat = lee.next();
		System.out.print("Apellido Materno: ");
		apeMat = lee.next();
		System.out.print("Numero de Tarjeta: ");
		numTarjeta = lee.next();
		System.out.print("NIP: ");
		nip = lee.nextInt();
		System.out.print("Fondo inicial: ");
		fondoInicial = lee.nextDouble();
		Cajero.crearUsaurio(nombre, apePat, apeMat, numTarjeta, nip, fondoInicial);
		System.out.println("Usuario creado");
	}
	
	private static void abrirCajero() {
		Cajero cajero = new Cajero();
		boolean CajeroAbierto = true;
		String numTarjeta = "";
		int nip = 0;
		
		System.out.println("------- Ingresa tu tarjeta -------");
		System.out.print("#Tarjeta: ");
		numTarjeta = lee.next();
		System.out.println("-------- Ingresa tu NIP  ---------");
		System.out.print("NIP: ");
		nip = lee.nextInt();
		if(cajero.validarTarjetaConNip(numTarjeta, nip)) {
			System.out.println("----------- Bienvenido -----------");
			System.out.println("---- " + cajero.consultaNombreUsuario() +" ----");
			do {
				imprimirMenuCajero();
				int resp = lee.nextInt();
				switch(resp) {
					case 1:
						System.out.println("TU SALDO ES: " + cajero.consultarFondos());
						break;
					case 2:
						retirarDinero(cajero);
						break;
					case 3:
						depositarDinero(cajero);
						break;
					case 4:
						cajero = null;
						CajeroAbierto = false;
						break;
					default:
						System.out.println("Esa opci�n no existe, si quieres salir elige 4");
				}
				System.out.print("�Desea realizar otra operaci�n? [S� => 5]");
				if(lee.nextInt() != 5) {
					cajero = null;
					CajeroAbierto = false;
				}
			}while(CajeroAbierto);
			
		}
	}
	
	private static void retirarDinero(Cajero cajero) {
		System.out.println("----------------------------------");
		System.out.print("------- Cantidad a Retirar [$]: ");
		double cantidadARetirar = lee.nextDouble();
		if(cajero.retirarFondos(cantidadARetirar)) {
			System.out.println("----------------------------------");
			System.out.println("======= Transacci�n exitosa ======");
			System.out.println("----------------------------------");
		}
		
	}
	
	private static void depositarDinero(Cajero cajero) {
		System.out.println("----------------------------------");
		System.out.print("----- Cantidad a Depositar [$]: ");
		double cantidadADepositar = lee.nextDouble();
		if(cajero.depositarDinero(cantidadADepositar)) {
			System.out.println("----------------------------------");
			System.out.println("======= Transacci�n exitosa ======");
			System.out.println("----------------------------------");
		}else {
			System.out.println("=> Ocurri� un error durante la transacci�n");
		}
	}
	
	private static void imprimirMenuPrincipal() {
		System.out.println("----------------------------------");
		System.out.println("---------- BANCO - MEN� ----------");
		System.out.println("----------------------------------");
		System.out.println("------- [1] Crear Usuario --------");
		System.out.println("------- [2] Ir a CAJERO   --------");
		System.out.println("------- [3] SALIR         --------");
		System.out.println("----------------------------------");
		System.out.print("----- Elige la opci�n que deseas: ");
	}
	
	private static void imprimirMenuCajero() {
		System.out.println("----------------------------------");
		System.out.println("---------- BANCO - CAJERO --------");
		System.out.println("----------------------------------");
		System.out.println("------- [1] Consultar Saldo  -----");
		System.out.println("------- [2] Retirar Dinero   -----");
		System.out.println("------- [3] Depositar Dinero -----");
		System.out.println("------- [4] SALIR            -----");
		System.out.println("----------------------------------");
		System.out.print(" Elige la operaci�n que deseas realizar: ");
	}
	
		
}
